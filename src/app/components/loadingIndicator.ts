import { LoadingController } from 'ionic-angular';
import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'indicator',
  template: `<div></div>`,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoadingIndicatorComponent {

  private _loading;

  constructor(
    public loadingCtrl: LoadingController
  ){
    
  }

  @Input()
  set loading(show: boolean){
    if(this._loading && !show){
      this._loading.dismiss();
      this._loading = null;
    }else if(show){
      this._loading = this.loadingCtrl.create({
        content: 'Please wait...'
      });

      this._loading.present();
    }
  }
  
}