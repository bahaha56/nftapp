import { LoadingIndicatorComponent } from './loadingIndicator';
import { IonicModule } from 'ionic-angular';
import { NotificationPreviewListComponent } from './notification-preview-list';
import { NotificationPreviewComponent } from './notification-preview';
import { NgModule } from '@angular/core';



export const COMPONENTS = [
  NotificationPreviewComponent,
  NotificationPreviewListComponent,
  LoadingIndicatorComponent
]

@NgModule({
  imports: [
    IonicModule,
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS
})
export class ComponentsModule{}