import { Notification } from './../models/notification';
import { Component, Input, ChangeDetectionStrategy } from '@angular/core';


@Component({
  selector: 'nft-preview',
  template: `
    <ion-item>
      <ion-icon name="leaf" item-left></ion-icon>
      <div>{{ receiveAt }}</div>
      <h2>{{ message }}</h2>
    </ion-item>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NotificationPreviewComponent {
  @Input() notification: Notification;

  private toMessage(type: string, item: string, unit: number, price: number){
    return `${type} ${item} with ${unit} at price: ${price}`;
  }

  get id(){
    return this.notification.id;
  }

  get receiveAt(){
    return this.notification.receive_at;
  }

  get message(){
    return this.toMessage(
      this.notification.type,
      this.notification.payload.item,
      this.notification.payload.unit,
      this.notification.payload.price
    );
  }
}