import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { Notification } from './../models/notification';


@Component({
  selector: 'nft-preview-list',
  template: `
    <ion-list no-lines>
      <nft-preview *ngFor="let nft of notifications" [notification]="nft"></nft-preview>
    </ion-list>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NotificationPreviewListComponent {
  @Input() notifications: Notification[];
}