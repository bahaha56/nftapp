export interface Notification {
  id: string,
  type: string,
  receive_at: Date,
  payload: {
    unit: number,
    item: string,
    price: number
  }
}