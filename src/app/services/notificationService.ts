import { Notification } from './../models/notification';
import { Observable } from 'rxjs/rx';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable()
export class NotificationService {

  constructor(private http: Http){}

  loadNotifications(): Observable<Notification[]> {
    return Observable.of([
      { id: '-3', type: 'sell', receive_at: new Date(), payload: {unit: 1, item: 'GOLD', price: 1234.5}},
      { id: '-1', type: 'buy', receive_at: new Date(2014, 4, 2), payload: {unit: 1, item: 'GOLD', price: 1235}},
      { id: '-2', type: 'close', receive_at: new Date(), payload: {unit: 1, item: 'CUR', price: 1234.9}}
    ]);
  }
}