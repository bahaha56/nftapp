import { Notification } from './../models/notification';
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs/rx';
import { NotificationService } from './../services/notificationService';
import { Injectable } from '@angular/core';

import _ from "lodash";

import { Effect, Actions } from '@ngrx/effects';

import * as notifications from './../actions/notifications';
import * as loading from './../actions/loadingIndicator';

@Injectable()
export class NotificationEffects {
  constructor(
    private action$: Actions,
    private nftSvc: NotificationService){}
    
  @Effect()
  loadNotifications$: Observable<Action> = this.action$
    .ofType(notifications.ActionTypes.LOAD)
    .startWith(new notifications.LoadAction())
    .switchMap(() => 
      this.nftSvc.loadNotifications()
        .map((nfts) => _.sortBy(nfts, ['receive_at']))
        .map((sortedNfts) => _.reverse(sortedNfts))
        .map((nfts: Notification[]) => new notifications.LoadSuccessAction(nfts))
    );
}