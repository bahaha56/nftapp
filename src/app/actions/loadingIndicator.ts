import { Action } from '@ngrx/store';
import { type } from './../../util';

export const ActionTypes = {
  SHOW_INDICATOR: type(`[Loading] Show Loading Indicator`),
  HIDE_INDICATOR: type(`[Loading] Hide Loading Indicator`)
};

export class ShowIndicatorAction implements Action {
  type = ActionTypes.SHOW_INDICATOR;
}

export class HideIndicatorAction implements Action {
  type = ActionTypes.HIDE_INDICATOR;
}

export type Actions
 = ShowIndicatorAction
  | HideIndicatorAction;