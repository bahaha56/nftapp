import { Notification } from './../models/notification';
import { Action } from '@ngrx/store';

import { type } from './../../util';

export const ActionTypes = {
  LOAD:         type('[Notifications] Load'),
  LOAD_SUCCESS: type('[Notifications] Load Success'),
  LOAD_FAIL:    type('[Notifications] Load Fail'),

  RECEIVE:      type('[Notifications] Recevice New Notification'),
};

export class LoadAction implements Action {
  type = ActionTypes.LOAD;

  constructor(){}
}

export class LoadSuccessAction implements Action {
  type = ActionTypes.LOAD_SUCCESS;

  constructor(public payload: Notification[]){}
}

export class LoadFailAction implements Action {
  type = ActionTypes.LOAD_FAIL;

  constructor(public payload: any) {}
}

export class ReceiveAction implements Action {
  type = ActionTypes.RECEIVE;

  constructor(public payload: Notification){}
}

export type Actions
 = LoadAction
  | LoadSuccessAction
  | LoadFailAction
  | ReceiveAction;