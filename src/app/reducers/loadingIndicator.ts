import { Action } from '@ngrx/store';

import * as loadingIndicator from '../actions/loadingIndicator';
import * as notifications from '../actions/notifications';

export interface State {
  showIndicator: boolean;
}

const initialState: State = {
  showIndicator: false
}

export function reducer(state = initialState, action: Action): State {
  switch(action.type){
    case '@ngrx/store/init':
    case notifications.ActionTypes.LOAD:
    case loadingIndicator.ActionTypes.SHOW_INDICATOR:
      return {
        showIndicator: true
      };

    case notifications.ActionTypes.LOAD_SUCCESS:
    case notifications.ActionTypes.LOAD_FAIL:
    case loadingIndicator.ActionTypes.HIDE_INDICATOR:
      return {
        showIndicator: false
      };

    default:
      return state;
  }
}

export const getShowIndicator = (state: State) => state.showIndicator;