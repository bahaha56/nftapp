import { enviroment } from './../../environment';

import { createSelector } from 'reselect';
import { combineReducers, ActionReducer } from '@ngrx/store';

import { compose } from '@ngrx/core/compose';

import { storeFreeze } from 'ngrx-store-freeze';

import * as fromLoadingIndicator from './loadingIndicator';
import * as fromNotifications from './notifications';

export interface State {
  indicatorLoading: fromLoadingIndicator.State;
  notifications: fromNotifications.State;
}

const reducers = {
  indicatorLoading: fromLoadingIndicator.reducer,
  notifications: fromNotifications.reducer,
};

const devReducer: ActionReducer<State> = compose(storeFreeze, combineReducers)(reducers);
const prodReducer: ActionReducer<State> = combineReducers(reducers);

export function reducer(state: any, action: any){
  if(enviroment.production){
    return prodReducer(state, action);
  }
  return devReducer(state, action);
}


/*
 * Layout Reducers
 */
export const getLayoutState = (state: State) => state.indicatorLoading;
export const getShowIndicator = createSelector(getLayoutState, fromLoadingIndicator.getShowIndicator);

export const getNotificationsState = (state: State) => state.notifications;

export const getNotificationLoaded = createSelector(getNotificationsState, fromNotifications.getLoaded);
export const getNotifications = createSelector(getNotificationsState, fromNotifications.getNfts);
