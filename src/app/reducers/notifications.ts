import { Notification } from './../models/notification';
import * as notifications from './../actions/notifications';

export interface State {
  loaded: boolean;
  nfts: Notification[]
}

const initialState: State = {
  loaded: false,
  nfts: []
}

export function reducer(state = initialState, action: notifications.Actions): State {
  switch(action.type){
    case notifications.ActionTypes.LOAD: {
      return Object.assign({}, state, { loading: true });
    }

    case notifications.ActionTypes.LOAD_SUCCESS: {
      const nfts = action.payload;
      return {
        loaded: true,
        nfts: nfts
      };
    }

    case notifications.ActionTypes.RECEIVE: {
      const nft = action.payload;
      return Object.assign({}, state, { nfts: [nft, ...state.nfts] });
    }

    default: {
      return state;
    }
  }
}

export const getLoaded = (state: State) => state.loaded;
export const getNfts = (state: State) => state.nfts;