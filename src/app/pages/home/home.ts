import { Notification } from './../../models/notification';
import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
import { Observable } from 'rxjs/rx';

import { NavController, LoadingController } from 'ionic-angular';

import { Store } from '@ngrx/store';

import * as fromRoot from '../../reducers';
import { ReceiveAction } from '../../actions/notifications';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomePage {
  notifications$: Observable<Notification[]>;
  loading$: Observable<boolean>;

  private _idCount;

  constructor(
    public navCtrl: NavController,
    private store: Store<fromRoot.State>
  ) {
    this.notifications$ = this.store.select(fromRoot.getNotifications);
    this.loading$ = this.store.select(fromRoot.getShowIndicator);

    this._idCount = 10;
  }

  receiveNft(){
    this.store.dispatch(new ReceiveAction({
      id: ''+(++this._idCount),
      type: 'sell',
      receive_at: new Date(),
      payload: {
        unit: 1,
        item: 'TEST',
        price: 100
      }
    }))
  }
}
